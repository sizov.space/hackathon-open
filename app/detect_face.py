from random import randint
from skimage import io
import cv2
import sys
import os
import time
import colorsys



def detect_faces(image_path):

    CASCADE="/home/ubuntu/licemer/app/Face_cascade.xml"
    FACE_CASCADE=cv2.CascadeClassifier(CASCADE)

    image_path = os.path.join('/home/ubuntu/', image_path)

    image=cv2.imread(image_path)
    image_grey=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

    print(image_grey)

    faces = FACE_CASCADE.detectMultiScale(image_grey,scaleFactor=1.16,minNeighbors=5,minSize=(25,25),flags=0)

    for x,y,w,h in faces:
        sub_img=image[y-10:y+h+10,x-10:x+w+10]
        cv2.rectangle(image,(x,y),(x+w,y+h),(255, 255,0),3)
        average = sub_img.mean(axis=0).mean(axis=0)
        average2=colorsys.rgb_to_hsv(average[0],average[1],average[2])
        h=average2[0]*60;
        h=((620-170)/(270*h));
        print(h)

if __name__ == "__main__":
	detect_faces("photo.jpg")
