from app import db

class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    color = db.Column(db.Float)
    url = db.Column(db.String(120), index=True, nullable=False)
    sex = db.Column(db.Integer)

class Celebrity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    face = db.Column(db.Float)
    clothes = db.Column(db.Float)
